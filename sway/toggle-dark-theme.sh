current_theme=$(gsettings get org.gnome.desktop.interface gtk-theme)
if [[ "$current_theme" == "'Adwaita'" ]]; then
	echo changing to dark theme
	gsettings set org.gnome.desktop.interface gtk-theme Adwaita-dark
	# Run gsd-xsettings to set theme for xwayland apps
	/usr/lib/gsd-xsettings --exit-time 1
elif [[ "$current_theme" == "'Adwaita-dark'" ]]; then
	echo changing to light theme
	gsettings set org.gnome.desktop.interface gtk-theme Adwaita
	# Run gsd-xsettings to set theme for xwayland apps
	/usr/lib/gsd-xsettings --exit-time 1
fi
