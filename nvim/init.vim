let g:plugged_home = '~/.vim/plugged'

" Edit default settings for yaml indentation (13-7-2020)
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType py setlocal ts=4 sts=4 sw=4 expandtab

" Plugins List

call plug#begin(g:plugged_home)
	" Airline as alternative statusline
	Plug 'vim-airline/vim-airline'
	Plug 'vim-airline/vim-airline-themes'
	" Alternative font coloring
	Plug 'chriskempson/base16-vim'
	" Add git support "
	Plug 'tpope/vim-fugitive'
	" LaTeX support through vimtex"
	Plug 'lervag/vimtex'
	" Linting by Ale "
	Plug 'dense-analysis/ale'
	" Autocompletion by YCM"
	Plug 'valloric/youcompleteme', {'do': './install.py'}
call plug#end()

" Airline settings
	let g:airline_theme='base16_atelierforest'
	let g:airline#extensions#tabline#enabled = 1
	let g:airline_powerline_fonts = 1
" Base 16 settings (Color scheme)
	let base16colorspace=256
	colorscheme base16-gruvbox-dark-hard
	set background=dark
	" True Color Support if it's avaiable in terminal
	if has("termguicolors")
	    set termguicolors
	endif

" Hide ~ signs for empty lines
	hi NonText guifg=bg
" Numbering settings
	set number
	set relativenumber

" Vimtex settings
	let g:tex_flavor = 'latex'

" Ale settings (Thanks Salt_Factory!)
	let g:ale_linters = {
		    \   'python': ['flake8', 'mypy'],
		    \   'tex': ['alex', 'lacheck', 'proselint', 'redpen'],
		    \   'html': ['alex', 'proselint'],
		    \   'restructuredtext': ['rstcheck', 'alex', 'redpen', 'proselint', 'write-good']
		    \}
	let g:ale_sign_column_always = 1

